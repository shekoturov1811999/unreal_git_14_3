
#include <iostream>
#include <string>

int main()
{
	
	std::string name;

	std::getline(std::cin, name);

	std::cout << "That is what you enter: " << name <<
		 "\nIt's length itself: " << name.length() <<
		 "\nFirst symbol: " << name.front() <<
		 "\nLast symbol: " << name.back() <<
		 std::endl;
		 

	return 0;

}

